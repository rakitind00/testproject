<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $casts = [
        'reviewers_ids' => 'array'
    ];
}
