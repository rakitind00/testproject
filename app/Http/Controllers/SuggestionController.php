<?php

namespace App\Http\Controllers;

use App\Http\Requests\SuggestionRequest;
use App\Publication;
use App\Reviewer;
use App\Suggestion;
use Illuminate\Http\Request;

class SuggestionController extends Controller
{
    public function asignPublicationForm()
    {
        return view('layouts\asign_publication', ['publications' => Publication::where('is_reviewed', false)->get()]);
    }

    public function asignPublication(SuggestionRequest $req)
    {
        session_start();
        $p_id = $req->input('publication_id');
        $q = $req->input('max_found');   

        $reviewers_arr = $this->processData($p_id, $q);
        $publication = Publication::find($p_id);
        return view('layouts\asign_publication',['publications' => Publication::where('is_reviewed', false)->get(), 'reviewers'=>$reviewers_arr, 'selected_p'=> $publication,'max_count'=>$q]);
    }

    public function asignPublicationById($id)
    {
        $p_id =$id;
        $q =3;   

        $reviewers_arr = $this->processData($p_id, $q);
        $publication = Publication::find($p_id);
        
        return  view('layouts\asign_publication',['publications' => Publication::where('is_reviewed', false)->get(), 'reviewers'=>$reviewers_arr, 'selected_p'=> $publication]);
    }

    public function saveData($id, Request $req){
        $reviewers_arr = $req->input('reviewers');
        $p_id= $req->input('selected_p');

        $reviewers_arr = preg_replace_callback(
            '!s:(\d+):"(.*?)";!', 
            function($m) { 
                return 's:'.strlen($m[2]).':"'.$m[2].'";'; 
            }, 
            $reviewers_arr);

        $reviewers_arr = unserialize($reviewers_arr);
        
       
        $suggestion = Suggestion::where('publication_id','=', $p_id)->first();
        $msg='This publication is already asigned, so it was updated';
        
        if($suggestion == null)
        {
            $suggestion = new Suggestion();
            $msg = 'Suggestion saved successfully';
        }
    
        $suggestion->publication_id = $p_id;
        $suggestion->reviewers_ids = array();
        $suggestion->reviewers_ids = array_merge($suggestion->reviewers_ids,  $reviewers_arr);

        $suggestion->save();
        return redirect()->route('asign-publication')->with('success',$msg); 
            // /['reviewers'=>$reviewers_arr, 'selected_p'=> $p_id]
    }

    public function processData($id, $max = 3)
    {
        $p_id = $id;
        $q = $max;

        $publication = Publication::find($p_id);
        $suggestion = new Suggestion();
        $suggestion->publication_id = $p_id;
        $reviwers = Reviewer::orderBy('pub_count', 'desc')->get();


        $arr = array();
        foreach ($reviwers as $reviwer) {
            $themes = in_array($publication->theme, $reviwer->themes);
            $lang = is_array($reviwer->languages) && in_array($publication->language, $reviwer->languages);

            $theme_index = array_search($publication->theme, $reviwer->themes);

            if ($themes && $lang) {
                $arr[] = collect(['reviewer' => $reviwer->id, 'theme_index' => $theme_index + 1])->all();
            } else {
                if ($publication->language == $reviwer->languages && $themes) {
                    $arr[] = collect(['reviewer' => $reviwer->id, 'theme_index' => $theme_index + 1])->all();
                }
            }
            if (count($arr) == $q) {
                break;
            }
        }
        $arr = collect($arr)->sortBy('theme_index');
        //dd($arr->all());


        $rev_ids = array();
        $reviewers_arr = array();
        foreach ($arr as $obj) {
            $reviewers_arr[] = Reviewer::find($obj['reviewer']);
            $rev_ids[] = $obj['reviewer'];
        }

       

        return $reviewers_arr;
    }
}
