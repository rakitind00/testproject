<?php

namespace App\Http\Controllers;

use App\asigned_publication as Asign;
use App\Publication;
use App\Reviewer;
use Illuminate\Http\Request;

class AsignController extends Controller
{
    public function asignReviewer(Request $req){
        $asign = new Asign();
        $asign->publication_id =  $req->input('publication_id');
        $asign->reviewers_id = $req->input('reviewer_id');

        $publication = Publication::find($req->input('publication_id'));
        $publication->reviewer_id =  $req->input('reviewer_id');

        $publication->save();
        $asign->save();

        return redirect()->route('asign-publication')->with('success', 'reviewer for this publication save successfully');

    }

    public function markAsReviewed($p_id, $rev_id){
      $publication = Publication::find($p_id);
      $publication->is_reviewed = true;
      $publication->save();
      
      $reviewer = Reviewer::find($rev_id);
      $reviewer->pub_count = $reviewer->pub_count+1;
      $reviewer->save();
      return redirect()->route('asign-publication')->with('success', 'publication is marked as reviewed now');

    }
}
