<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PublicationRequest;
use App\Publication as AppPublication;

class PublicationsController extends Controller
{
    public function newPublicationForm()
    {
        $languages = ["Chinese", "Spanish", "English", "Russian", "Japanese", "French", "Ukrainian"];

        $themes = ["Science and Knowledge", "Philosophy. Psychology", "Religion. Theology", " Social Sciences", "vacant", "Mathematics. Natural Sciences", "Applied Sciences. Medicine, Technology", "The Arts. Entertainment. Sport", " Linguistics. Literature", "Geography. History"];
        return view('layouts\post-publication', ['themes' => $themes, 'languages' => $languages]);
    }

    public function newPublicationSubmit(PublicationRequest $req)
    {
        // $validation = $req->validate(['name'=>'required|min:3|max:50', ]);

        $publication = new AppPublication();
        $publication->name = $req->input('name');
        $publication->author = $req->input('author');
        $publication->key_words = $req->input('key_words');
        $publication->theme = $req->input('theme');
        $publication->anotation = $req->input('anotation');
        $publication->language = $req->input('language');

        $publication->save();

        return redirect()->route('publication-list')->with('success', 'Publication created successfully');
    }

    public function getAll()
    {
        //$publication = new AppPublication;
        //return view('layouts\publications',['data'=> $publication->orderBy('id','asc')->get()]); //sort all by id asc
        //return view('layouts\publications',['data'=> $publication->orderBy('id','desc')->take(5)->get()]); 
        //sort all by id desc and get just 5 objects
        //->skip(n) - skip first found n objects
        //publication->where('name' ,'=', 'Some name')->get()   *PS: use expression != like this: <>

        return view('layouts\publications', ['data' => AppPublication::all()]);
    }

    public function getPublication($id)
    {
        return view('layouts\publication-info', ['data' => AppPublication::find($id)]);
    }

    public function editPublication($id)
    {
        $languages = ["Chinese", "Spanish", "English", "Russian", "Japanese", "French", "Ukrainian"];
        $themes = ["Science and Knowledge", "Philosophy. Psychology", "Religion. Theology", " Social Sciences", "vacant", "Mathematics. Natural Sciences", "Applied Sciences. Medicine, Technology", "The Arts. Entertainment. Sport", " Linguistics. Literature", "Geography. History"];
        return view('layouts\publication-edit', ['data' => AppPublication::find($id), 'themes' => $themes, 'languages' => $languages ]);
    }

    public function saveEditedPublication($id, PublicationRequest $req)
    {
        // $validation = $req->validate(['name'=>'required|min:3|max:50', ]);

        $publication = AppPublication::find($id);
        $publication->name = $req->input('name');
        $publication->author = $req->input('author');
        $publication->key_words = $req->input('key_words');
        $publication->theme = $req->input('theme');
        $publication->anotation = $req->input('anotation');

        $publication->save();

        return redirect()->route('publication-info', $id)->with('success', 'Publication updated successfully');
    }

    public function deletePublication($id)
    {
        AppPublication::find($id)->delete();
        return redirect()->route('publication-list', $id)->with('success', 'Publication deleted successfully');
    }
}
