<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewerRequest;
use App\Reviewer;
use Illuminate\Http\Request;

class ReviewersController extends Controller
{

    public function newReviewerForm(){
        $themes=["Science and Knowledge","Philosophy. Psychology", "Religion. Theology", " Social Sciences", "vacant", "Mathematics. Natural Sciences", "Applied Sciences. Medicine, Technology","The Arts. Entertainment. Sport", " Linguistics. Literature", "Geography. History"];
        $languages = ["Chinese", "Spanish", "English", "Russian", "Japanese", "French", "Ukrainian"];

        return view('layouts\register-reviewer', ['themes' => $themes, 'languages'=>$languages ]);
    }

    public function newReviewerSubmit(ReviewerRequest $req)
    {
        // $validation = $req->validate(['name'=>'required|min:3|max:50', ]);
        $reviewer = new Reviewer();
        $reviewer->first_name = $req->input('first_name');
        $reviewer->last_name = $req->input('last_name');
        $reviewer->themes = $req->input('themes');
        $reviewer->languages = $req->input('languages');
        $reviewer->pub_count = 0;


        $reviewer->save();

        return redirect()->route('reviewer-list')->with('success', 'reviewer created successfully');
    }


    public function getAll()
    {
        return view('layouts\reviewers', ['data' => Reviewer::all()]);
    }

    public function getReviewer($id)
    {
        return view('layouts\reviewer-info', ['data' => Reviewer::find($id), ]);
    }

    public function editReviewer($id)
    {
        $themes=["Science and Knowledge","Philosophy. Psychology", "Religion. Theology", " Social Sciences", "vacant", "Mathematics. Natural Sciences", "Applied Sciences. Medicine, Technology","The Arts. Entertainment. Sport", " Linguistics. Literature", "Geography. History"];
        $languages = ["Chinese", "Spanish", "English", "Russian", "Japanese", "French", "Ukrainian"];

        return view('layouts\reviewer-edit', ['data' => Reviewer::find($id), 'themes'=>$themes, 'languages'=>$languages]);
    }

    public function saveEditedReviewer($id,ReviewerRequest $req)
    {   
        // $validation = $req->validate(['name'=>'required|min:3|max:50', ]);
        $reviewer = Reviewer::find($id);
        $reviewer->first_name = $req->input('first_name');
        $reviewer->last_name = $req->input('last_name');
        $reviewer->themes = $req->input('themes');
        $reviewer->languages = $req->input('languages');

        $reviewer->save();

        return redirect()->route('reviewer-info',$id)->with('success', 'reviewer updated successfully');
    }

    public function deleteReviewer($id){
        Reviewer::find($id)->delete();
        return redirect()->route('reviewer-list', $id)->with('success', 'Reviewer deleted successfully');
    }
}
