<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $casts = [
        'themes' => 'array',
        'languages' => 'array'
    ];
}
