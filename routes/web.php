<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use function GuzzleHttp\Promise\all;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');;

//Publications
Route::get('/publications/new', 'PublicationsController@newPublicationForm')->name('new-publication-form');

Route::post('/publications/new/submit', 'PublicationsController@newPublicationSubmit')->name('new-publication-submit');

Route::get('/publications', 'PublicationsController@getAll')->name('publication-list');

Route::get('/publications/{id}', 'PublicationsController@getPublication')->name('publication-info');

Route::get('/publications/{id}/update', 'PublicationsController@editPublication')->name('publication-edit');
Route::post('/publications/{id}/update', 'PublicationsController@saveEditedPublication')->name('publication-edit');

Route::get('/publications/{id}/delete', 'PublicationsController@deletePublication')->name('publication-delete');

//Reviewers
Route::get('/reviewers/new','ReviewersController@newReviewerForm' )->name('new-reviewer');

Route::post('/reviewers/new/submit', 'ReviewersController@newReviewerSubmit')->name('new-reviewer-submit');

Route::get('/reviewers', 'ReviewersController@getAll')->name('reviewer-list');

Route::get('/reviewers/{id}', 'ReviewersController@getReviewer')->name('reviewer-info');

Route::get('/reviewers/{id}/update', 'ReviewersController@editReviewer')->name('reviewer-edit');
Route::post('/reviewers/{id}/update', 'ReviewersController@saveEditedReviewer')->name('reviewer-edit');

Route::get('/reviewers/{id}/delete', 'ReviewersController@deleteReviewer')->name('reviewer-delete');

//Suggestions
Route::get('/asign-publication','SuggestionController@asignPublicationForm' )->name('asign-publication');
Route::post('/asign-publication','SuggestionController@asignPublication' )->name('asign-publication-submit');
Route::get('/asign-publication/{id}','SuggestionController@asignPublicationById' )->name('asign-publication-id');
Route::post('/asign-publication/{id}/save','SuggestionController@saveData' )->name('asign-publication-save');

//saved reviewers (Asign)
Route::post('/asign-reviewer/publication/{id}','AsignController@asignReviewer')->name('asign-reviewer');