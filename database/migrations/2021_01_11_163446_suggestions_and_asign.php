<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SuggestionsAndAsign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('publication_id')->unsigned()->index();
            $table->json('reviewers_ids');
            $table->foreign('publication_id')->references('id')->on('publications')->onDelete('cascade');;
            $table->softDeletes();
        });

        Schema::create('asigned_publications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('publication_id')->unsigned()->index();
            $table->bigInteger('reviewers_id')->unsigned()->index();
            $table->foreign('publication_id')->references('id')->on('publications')->onDelete('cascade');;
            $table->foreign('reviewers_id')->references('id')->on('asigned_publications')->onDelete('cascade');;
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
