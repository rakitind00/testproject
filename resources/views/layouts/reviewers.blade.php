@extends('../app')

@section('title')
Reviewers
@endsection

@section('content')
<div class="container">
    <h1>Reviewers</h1>

    @foreach($data as $obj)
    <div class="alert alert-info">
        <h3> #{{$obj->id}}. {{ $obj->first_name }} {{ $obj->last_name }}</h3>
        <a href="{{route('reviewer-info', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-primary" > Details</button>
        </a>

        <a href="{{route('reviewer-edit', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-warning"> EDIT INFO</button>
        </a>

        <a href="{{route('reviewer-delete', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-danger"> DELETE </button>
        </a>

    </div>
    @endforeach
@endsection