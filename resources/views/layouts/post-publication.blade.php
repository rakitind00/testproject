@extends('../app')

@section('title')
New publication
@endsection

@section('content')
<div class="shadow card container">
    <h1>New publication</h1>

    <form action="{{ route('new-publication-submit') }}" method="post">
        @csrf

        <div class="form-group">
            <label> Enter publication title </label>
            <input type="text" name="name" placeholder="title" id="title" class="form-control" />
        </div>

        <div class="form-group">
            <label> Author name</label>
            <input type="text" name="author" placeholder="author" id="author" class="form-control" />
        </div>

        <div class="form-group">
            <label> Theme </label>
            <select name="theme" placeholder="theme" id="theme" class="form-control">
            
                <option selected disabled > #select theme</option>
           
                @foreach($themes as $key=>$theme)
                    <option id="$key" value="{{$theme}}"> {{$theme}} </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label> Language </label>
            <select type="text" name="language" placeholder="language" id="language" class="form-control" >
            <option selected disabled > #select language</option>
                @foreach($languages as $lang)
                    <option value="{{$lang}}" > {{$lang}} </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label> Key words </label>
            <input type="text" name="key_words" placeholder="key_words" id="key_words" class="form-control" />
        </div>

        <div class="form-group">
            <label> Anotation </label>
            <textarea type="text" name="anotation" placeholder="anotation" id="anotation" class="form-control"></textarea>
        </div>

        <button type="submit" class="btn btn-success mt-3 mb-3"> Post publication</button>
    </form>
</div>
@endsection