@extends('../app')

@section('title')
Asign publication
@endsection

@section('content')
<div class="container">

    <h1>Asign Publication </h1>
    <label>Choose publication and we will seek for compatible reviewers</label><br>

    <div class="shadow card mt-3 p-3">
        <form action="{{ route('asign-publication-submit') }}" method="post">
            @csrf
            <div class="row">
                <div class="w-25">
                    <label class="text-secondary">Publication:</label><br>
                    <select class="form-control" name="publication_id">
                        <option selected disabled> select publication </option>
                        @foreach($publications as $publication)
                            <option value={{$publication->id}}
                                @if( $selected_p ?? '' )
                                    @if($selected_p->id == $publication->id)
                                        selected 
                                    @endif
                                @endif> 

                                {{ $publication->name}} 
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="w-50">
                    <label class="text-secondary"> Select max count of suggested people to be found: </label>
                    <select name="max_found" class="form-control w-25">
                        @for($i=1; $i <= 15; $i++) 
                            <option
                                @if($i===3 ) selected @endif  
                                @if(isset($max_count) && $max_count == $i) selected @endif>
                                {{ $i }}
                            </option>
                        @endfor
                    </select>
                </div>
            </div>



            <button type="submit" class="btn mt-3 mb-3 rounded border border-success ">Find </button>

        </form>
    </div>

    @include('./inc.found_reviewers')

</div>

@endsection