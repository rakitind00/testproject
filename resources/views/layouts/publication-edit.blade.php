@extends('../app')

@section('title')
Edit publication
@endsection

@section('content')
<div class="shadow card container">
    <h1>Edit publication</h1>

    <form action="{{ route('publication-edit', $data->id) }}" method="post">
        @csrf

        <div class="form-group">
            <label> Enter publication title </label>
            <input type="text" name="name" value="{{$data->name}}" placeholder="title" id="title" class="form-control" />
        </div>

        <div class="form-group">
            <label> Author name</label>
            <input type="text" name="author" value="{{$data->author}}" placeholder="author" id="author" class="form-control" />
        </div>

        <div class="form-group">
            <label> Theme </label>
            <select class="form-control" name="theme" id="theme">
                <option selected disabled > #select theme</option>

                @foreach($themes as $key=>$theme)
                    <option id="$key" value="{{$theme}}"  @if( strcmp($theme, ($data->theme)) == 0) selected @endif> {{$theme}} </option>
                @endforeach
            </select>

        </div>

        <div class="form-group">
            <label> Key words </label>
            <input type="text" name="key_words" value="{{$data->key_words}}" placeholder="key_words" id="key_words" class="form-control" />
        </div>

        
        <div class="form-group">
            <label> Language </label>
            <select type="text" name="language" value="{{$data->language}}" placeholder="language" id="language" class="form-control" >
                @foreach($languages as $lang)
                    <option value="$lang"  @if( strcmp($lang, ($data->language)) == 0) selected @endif> {{$lang}} </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label> Anotation </label>
            <textarea type="text" name="anotation" placeholder="anotation" id="anotation" class="form-control">{{$data->anotation}}</textarea>
        </div>

        <button type="submit" class="btn btn-success mt-3 mb-3"> Save </button>

    </form>
    <a href="{{route('publication-info', $data->id )}}">
        <button class="btn btn-light ms-5"
            style="position: absolute; left: 7em; bottom: 1em;">Cancell
        </button>
    </a>


</div>
@endsection