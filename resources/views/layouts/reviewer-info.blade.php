@extends('../app')

@section('title')
Reviewer info
@endsection

@section('content')
<div class="container">

    <h1>Reviewer #{{$data->id}} </h1>

    <div class="alert alert-info">
        <label> First name : {{ $data->first_name }} </label><br>
        <label> Last name: {{ $data->last_name }} </label><br>
        <label> Themes:
            @foreach( $data->themes as $key=>$theme)
            {{ $key+1  }}) {{ $theme }}
            @endforeach
        </label>
        <br>
        <label> Languages:
            @foreach( $data->languages as $key=>$lang)
                {{ $key+1  }}) {{ $lang }}
            @endforeach
        </label><br>

        <label> Reviews count: {{ $data->pub_count }} </label><br>


        <a href="{{route('reviewer-edit', $data->id)}}" class="text-decoration-none">
            <button class="btn btn-warning"> EDIT INFO</button>
        </a>

        <a href="{{route('reviewer-delete', $data->id)}}" class="text-decoration-none">
            <button class="btn btn-danger"> DELETE </button>
        </a>

    </div>
</div>

@endsection