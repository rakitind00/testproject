@extends('../app')

@section('title')
New reviwer
@endsection

@section('content')
<div class="shadow card container">
    <h1>Register reviewer</h1>

    <form action="{{ route('new-reviewer-submit') }}" method="post">
        @csrf

        <div class="form-group">
            <label> Reviewer first name </label>
            <input type="text" name="first_name" placeholder="first name" id="first_name" class="form-control" />
        </div>

        <div class="form-group">
            <label>Reviewer last name </label>
            <input type="text" name="last_name" placeholder="last name" id="last_name" class="form-control" />
        </div>

        <div class="form-group mt-2">
            <label> Themes </label>
            <select name="themes[]" id="themes" multiple>
                <option disabled selected> #select theme(s) </option>
                @foreach($themes as $key=>$theme)
                    <option id="$key" value="{{$theme}}"> {{$theme}} </option>
                @endforeach
            </select>
        </div>


        <div class="form-group mt-2">
            <label> Languages </label>
            <select name="languages[]" id="languages" multiple>
                <option disabled selected> #chose language(s) </option>
                @foreach($languages as $key=>$language)
                    <option id="$key" value="{{$language}}"> {{$language}} </option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-success mt-3 mb-3"> Save </button>
    </form>
</div>
@endsection