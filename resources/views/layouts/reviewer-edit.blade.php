@extends('../app')

@section('title')
Edit reviwer
@endsection

@section('content')
<div class="shadow card container">
    <h1>Edit reviewer</h1>

    <form action="{{ route('reviewer-edit', $data->id) }}" method="post">
        @csrf

        <div class="form-group">
            <label> Reviewer first name </label>
            <input type="text" value="{{$data->first_name}}" name="first_name" placeholder="first name" id="first_name" class="form-control" />
        </div>

        <div class="form-group">
            <label>Reviewer last name </label>
            <input type="text" value="{{$data->last_name}}" name="last_name" placeholder="last name" id="last_name" class="form-control" />
        </div>

        <div class="form-group mt-2">
            <label> Themes </label>
            <select name="themes[]" multiple>
                @foreach($themes as $key=>$theme)
                <option id="$key" value="{{$theme}}" @if(in_array($theme, $data->themes )) selected @endif> {{$theme}} </option>
                @endforeach
            </select>

        </div>


        <div class="form-group mt-2">
            <label> Languages </label>
            <select name="languages[]" multiple>
                @foreach($languages as $key=>$language)
                <option id="$key" value="{{$language}}" @if(is_array( $data->languages) && in_array($language, $data->languages ))
                    selected
                    @else
                    @if ( $data->languages == $language)
                    selected
                    @endif
                    @endif> {{$language}} </option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-success mt-3 mb-3"> Save </button>

    </form>
    <a href="{{route('reviewer-info', $data->id )}}"><button class="btn btn-light" 
    style="position: absolute; left: 7em; bottom: 1em;">Cancell</button></a>




</div>
@endsection