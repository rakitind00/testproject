@extends('../app')

@section('title')
Publication info
@endsection

@section('content')
<div class="container">

    <h1>Publication #{{$data->id}} </h1>

    <div class="alert alert-info">
        <h3> Title: {{ $data->name }} </h3>
        <label> Author: {{ $data->author }} </label><br>
        <label> Theme: {{$data->theme}} </label><br>
        <label> Key_words: {{$data->key_words}} </label><br>
        <label> Anotation: {{$data->anotation}} </label><br>
        <label> Language: {{$data->language}} </label><br>

        <a href="{{route('publication-edit', $data->id)}}" class="text-decoration-none">
            <button class="btn btn-warning"> EDIT INFO</button>
        </a>

        <a href="{{route('publication-delete', $data->id)}}" class="text-decoration-none">
            <button class="btn btn-danger"> DELETE </button>
        </a>

        <a href="{{route('asign-publication-submit', $data->id)}}" class="text-decoration-none">
            <button class="btn btn-success"> Asign reviewer </button>
        </a>
    </div>
</div>

@endsection