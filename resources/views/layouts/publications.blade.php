@extends('../app')

@section('title')
Publications
@endsection

@section('content')
<div class="container">
    <h1>Publications</h1>

    @foreach($data as $obj)
    <div class="alert alert-info">
        <h3> #{{$obj->id}}. {{ $obj->name }} </h3>
        <label> Author: {{ $obj->author }} </label><br>
        <a href="{{route('publication-info', $obj->id)}}"  class="text-decoration-none">
            <button class="btn btn-primary"> Details</button>
        </a>

        <a href="{{route('publication-edit', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-warning"> EDIT INFO</button>
        </a>

        <a href="{{route('publication-delete', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-danger"> DELETE </button>
        </a>

        <a href="{{route('asign-publication-id', $obj->id)}}" class="text-decoration-none">
            <button class="btn btn-success"> Asign reviewer </button>
        </a>
    </div>
    @endforeach
    @endsection