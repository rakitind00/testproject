<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        .full-height {
            height: 100vh;
        }

        .flex-top {

            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
            padding-top: 5em;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
            color: #636b6f;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        a:hover {

            font-weight: 1000;
            color: black;
            box-shadow: 0px 10px 10px -8px #888888;

        }
    </style>
</head>

<body>
    <div class="flex-top position-ref full-height ">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md "  style="color:#17a2b8">
                Publication-Manager 
            </div>

            <div class="links">
                <a href="{{route('publication-list')}}">All publications</a>
                <a href="{{route('new-publication-form')}}">New publication</a>
                <a href="{{route('asign-publication')}}">Asign publication</a>

            </div>

            <div class="links" style="margin-top:20px;">
                <a href="{{route('reviewer-list')}}">Reviewers list</a>
                <a href="{{route('new-reviewer')}}">Register reviewer</a>
            </div>
        </div>
    </div>
</body>

</html>