@if(session('reviewers') && session('publication')  )
    <div class="shadow alert alert-info">
        <h4>{{session('publication')->name}}</h4>
        <label>Author: {{session('publication')->author}} </label><br>
        <label> Theme: {{session('publication')->theme}} </label><br>
        <label> Language: {{session('publication')->language}} </label>
    </div>
    @foreach( session('reviewers') as $reviewer)
        <div class="shadow card mb-1 mt-1 p-2">
            <h3> {{$reviewer->first_name}} {{$reviewer->last_name}}</h3>
            <label>Reviews count: {{$reviewer->pub_count}} </label>
     

            <label> Themes: @foreach( $reviewer->themes as $key=>$theme){{ $key+1  }}) {{ $theme }} @endforeach </label>
            <label> Languages: @if(is_array($reviewer->languages)) @foreach( $reviewer->languages as $key=>$lang) {{ $key+1  }}) {{ $lang }} @endforeach
                    @else  {{$reviewer->languages}} @endif
            </label>
        </div>
    @endforeach
    <button type="submit" class="btn btn-success mt-3 mb-3">Save this suggestion </button>

@endif


@if($reviewers ?? '')
    <div class="shadow alert alert-info">
        <h4>{{ $selected_p->name}}</h4>
        <label>Author: {{$selected_p->author}} </label><br>
        <label> Theme: {{$selected_p->theme}} </label><br>
        <label> Language: {{$selected_p->language}} </label>
    </div>
    @foreach( $reviewers ?? ''  as $reviewer)
        <div class="shadow card mb-1 mt-1 p-2">
            <h3> {{$reviewer->first_name}} {{$reviewer->last_name}}</h3>
            <label>Reviews count: {{$reviewer->pub_count}} </label>
     

            <label> Themes: @foreach( $reviewer->themes as $key=>$theme){{ $key+1  }}) {{ $theme }} @endforeach </label>
            <label> Languages: @if(is_array($reviewer->languages)) @foreach( $reviewer->languages as $key=>$lang) {{ $key+1  }}) {{ $lang }} @endforeach
                    @else  {{$reviewer->languages}} @endif
            </label>
            <form action="{{ route('asign-reviewer', $selected_p->id) }}" method="post">
            @csrf
                <input type="hidden" name="publication_id" value="{{$selected_p->id}}"/>
                <input type="hidden" name="reviewer_id" value="{{$reviewer->id}}"/>
                <button class="btn" type="submit" > asign this reviewer </button>
            </form>
        </div>
    @endforeach
   
    <form action="{{ route('asign-publication-save', $selected_p->id) }}" method="post">
        @csrf
        <input type="hidden" name="selected_p" value="{{$selected_p->id}}"/>
        <input type="hidden" name="reviewers"
        value="<?php  
            $ids=array();
            foreach($reviewers as $obj){
                $ids[] = $obj->id;
            }
            
            echo htmlspecialchars(serialize($ids)) ?>"/>

        <button class="btn btn-success" type="submit"> Save this suggestion </button>
    </form>
@endif
