<div class="container rounded-bottom shadow-lg bg-secondary bg-gradient p-1" >
    <nav role="full-horizontal d-flex align-items-center">
        <ul class="m-1" >
            <a href="{{route('home')}}" class="text-decoration-none"> <button type="button" class=" btn btn-outline-light"> Home </button> </a>
            <a href="{{route('publication-list')}}"class="text-decoration-none" > <button type="button" class="btn btn-outline-light"> All publications </button></a>
            <a href="{{route('new-publication-form')}}"class="text-decoration-none"> <button type="button" class="btn btn-outline-light"> New publication </button> </a>

            <a href="{{route('reviewer-list')}}" class="text-decoration-none"><button type="button" class="btn btn-outline-light"> Reviewers list </button> </a>
            <a href="{{route('new-reviewer')}}"class="text-decoration-none"> <button type="button" class="btn btn-outline-light"> Register reviewer </button> </a>
            <a href="{{route('asign-publication')}}" class="text-decoration-none"> <button class="btn btn-outline-light"> Asign publication</button></a>
        </ul>
    </nav>
</div>